-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 06-Jun-2021 às 20:06
-- Versão do servidor: 10.4.19-MariaDB
-- versão do PHP: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `avivatec`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `img`, `descricao`) VALUES
(1, 'Rick e Morty Clássico', 'https://images.tcdn.com.br/img/img_prod/697730/adesivo_lateral_vidro_caminhao_carro_decorativo_rick_and_morty_1147486360_1_20201210150106.jpg', 'Rick tira Morty da cama e o leva para uma viagem em outra dimensão. No caminho, ele fala sobre explodir a humanidade e começar o mundo do zero. Eles vão em busca de sementes para mega-árvores. Enquanto isso, na Terra, Jerry e Beth tem uma briga sobre como'),
(2, 'Rick e Morty Curtindo o Verão', 'https://www.planocritico.com/wp-content/uploads/2018/10/rick_and_morty_temporada_1_plano_critico.jpg', 'Depois de Jerry reclamar obre a inteligência de Snuffles, o cão da família, Rick coloca um tipo de capacete no cachorro para que ele fique mais esperto. Rick e Morty entram nos sonhos do professor de matemática para tentar convencê-lo a dar uma nota boa a'),
(3, 'Rick e Morty Fliperama', 'https://a-static.mlcdn.com.br/1500x1500/livro-rick-morty-volume-1/paninibrasil/30434/ec886981348f76380682d1e45fb4dd0e.jpg', 'Durante este episódio de Natal, Jerry vê seus planos de um feriado perfeito em família serem “estragados” ao conhecer o novo namorado de seus pais.'),
(4, 'Rick e Morty Sabugueiro', 'http://s2.glbimg.com/fP8FcBdhKXgZ2HCLdGl7R8MIHIk=/e.glbimg.com/og/ed/f/original/2017/10/20/rick-and-morty3.png', 'A realidade é corrompida pelos Zigerions, golpistas intergalácticos que colocam Rick em Morty em uma simulação. Os dois planejam um show de rap e Rick consegue travar o CPU ao ordenar que a platéia realize inúmeros movimentos.'),
(5, 'Rick e Morty PsyOps', 'https://files.nsctotal.com.br/s3fs-public/styles/itapema_blog_post_header/public/graphql-upload-files/rick%20and%20morty%20temporada%205_1.jpg?GBrFPeUxa5Sw4.bH_nosKtakjaTXr4LR&itok=z0sKO-XR', 'Morty pede a Rick uma poção para fazer Jéssica se apaixonar por ele. Rick entrega ao neto, mas esquece de avisá-lo de um pequeno detalhe: ela não pode estar gripada. '),
(6, 'Rick e Morty Projeto', 'https://cdn.mensagenscomamor.com/content/images/m000543388.jpg?v=1&w=302&h=167', 'Summer consegue um novo emprego em uma loja que é comandada pelo Diabo. Jerry e Morty discutem se Plutão é realmente um planeta e decidem ir até lá.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `viagem`
--

CREATE TABLE `viagem` (
  `id` int(11) NOT NULL,
  `origem` varchar(50) DEFAULT NULL,
  `destino` varchar(50) DEFAULT NULL,
  `usuarioId` int(11) DEFAULT NULL,
  `dataCriacao` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `viagem`
--

INSERT INTO `viagem` (`id`, `origem`, `destino`, `usuarioId`, `dataCriacao`) VALUES
(1, 'Sem Origem', 'Minas Gerais', 1, NULL),
(2, 'Sem Origem', 'São Paulo', 2, NULL),
(3, 'Itajubá', 'São Paulo', 1, NULL),
(9, 'Centro', 'BPS', 2, NULL),
(10, 'Itajubá', 'São Paulo', 4, NULL),
(11, 'Itajubá', 'São Paulo', 3, NULL),
(12, 'New York', 'Brasil', 2, NULL),
(13, 'São Paulo', 'Brasília', 1, NULL),
(14, 'Itajubá', 'São Paulo', 1, NULL),
(15, 'São Paulo', 'Itajubá', 1, NULL),
(16, 'Itajubá', 'São Paulo', 1, NULL),
(17, 'São Paulo', 'Itajubá', 1, NULL),
(18, 'Itajubá', 'São Paulo', 1, NULL),
(19, 'São Paulo', 'Itajubá', 1, NULL),
(20, 'Itajubá', 'São Paulo', 1, NULL),
(21, 'São Paulo', 'Itajubá', 1, NULL),
(22, 'Itajubá', 'São Paulo', 1, NULL),
(23, 'Itajubá', 'São Paulo', 5, NULL),
(24, 'São Paulo', 'Ribeirão', 1, NULL),
(25, 'SP', 'MG', 1, '2021-06-06'),
(26, NULL, NULL, 4, '2021-06-06'),
(27, 'Casa do Rafa', 'Casa do Gabs', 1, '2021-06-06');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `viagem`
--
ALTER TABLE `viagem`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuarioId` (`usuarioId`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `viagem`
--
ALTER TABLE `viagem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `viagem`
--
ALTER TABLE `viagem`
  ADD CONSTRAINT `viagem_ibfk_1` FOREIGN KEY (`usuarioId`) REFERENCES `usuario` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
