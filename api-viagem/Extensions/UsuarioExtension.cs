﻿using api_viagem.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace api_viagem.Extensions
{
    public static class UsuarioExtension
    {
        public static IQueryable<Usuario> AplicaFiltro(this IQueryable<Usuario> query, UsuarioFilter filtro)
        {
            if (filtro != null)
            {
                if (filtro.Id > 0)
                {
                    query = query.Where(u => u.Id == filtro.Id);
                }
                if (filtro.Nome != null && !filtro.Nome.Equals(""))
                {
                    query = query.Where(u => u.Nome.Contains(filtro.Nome));
                }
                if (filtro.Includes != null && filtro.Includes.Count() > 0)
                {
                    foreach (string s in filtro.Includes)
                        query = query.Include(s);
                }
            }

            return query;
        }

        public class UsuarioFilter
        {
            public int Id { get; set; }
            public string Nome { get; set; }
            public IEnumerable<string> Includes { get; set; }

        }
    }
}
