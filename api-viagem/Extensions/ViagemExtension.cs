﻿using api_viagem.Models;
using System.Collections.Generic;
using System.Linq;

namespace api_viagem.Extensions
{
    public static class ViagemExtension
    {
        public static IQueryable<Viagem> AplicaFiltro(this IQueryable<Viagem> query, ViagemFilter filtro)
        {
            if (filtro != null)
            {
                if (filtro.Id > 0)
                {
                    query = query.Where(u => u.Id == filtro.Id);
                }
                if (filtro.UsuarioId > 0)
                {
                    query = query.Where(u => u.UsuarioId == filtro.UsuarioId);
                }
                if (filtro.Destino != null && !filtro.Destino.Equals(""))
                {
                    query = query.Where(u => u.Destino.Contains(filtro.Destino));
                }
            }

            return query;
        }

        public class ViagemFilter
        {
            public int Id { get; set; }
            public int? UsuarioId { get; set; }
            public string Destino { get; set; }
            public IEnumerable<string> Includes { get; set; }
        }
    }
}
