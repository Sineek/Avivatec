﻿using api_viagem.Domains.Interfaces;
using api_viagem.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using static api_viagem.Extensions.ViagemExtension;

namespace api_viagem.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ViagemController : ControllerBase
    {
        private readonly ILogger<ViagemController> _logger;
        private readonly Contexto _context;
        private readonly IViagemDomain _domain;

        public ViagemController(ILogger<ViagemController> logger, Contexto context, IViagemDomain domain)
        {
            _logger = logger;
            _context = context;
            _domain = domain;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetViagens([FromQuery] ViagemFilter filtro)
        {
            _logger.LogInformation("Rota {Endpoint} acionada", nameof(GetViagens));

            try
            {
                _logger.LogInformation("Rota {EndPoint} acionada", nameof(GetViagens));
                var resp = await _domain.GetViagemByUserId(filtro);

                return Ok(resp);
            }
            catch (System.NullReferenceException)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (System.TimeoutException)
            {
                return StatusCode(StatusCodes.Status408RequestTimeout);
            }
            catch (Exception)
            {
                return BadRequest();
            }
            finally
            {
                _logger.LogInformation("Rota {Endpoint} finalizada", nameof(GetViagens));
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostViagem([FromBody] Viagem viagem)
        {
            _logger.LogInformation("Rota {Endpoint} acionada", nameof(PostViagem));

            try
            {
                _logger.LogInformation("Rota {EndPoint} acionada", nameof(PostViagem));
                var resp = await _domain.AddViagem(viagem);

                return Ok(resp);
            }
            catch (System.NullReferenceException)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (System.TimeoutException)
            {
                return StatusCode(StatusCodes.Status408RequestTimeout);
            }
            catch (Exception)
            {
                return BadRequest();
            }
            finally
            {
                _logger.LogInformation("Rota {Endpoint} finalizada", nameof(PostViagem));
            }
        }
    }
}
