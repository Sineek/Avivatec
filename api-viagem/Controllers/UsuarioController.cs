﻿using api_viagem.Domains.Interfaces;
using api_viagem.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using static api_viagem.Extensions.UsuarioExtension;

namespace api_viagem.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsuarioController : ControllerBase
    {
        private readonly ILogger<UsuarioController> _logger;
        private readonly Contexto _context;
        private readonly IUsuarioDomain _domain;

        public UsuarioController(ILogger<UsuarioController> logger, Contexto context, IUsuarioDomain domain)
        {
            _logger = logger;
            _context = context;
            _domain = domain;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetUsuarios([FromQuery] UsuarioFilter filtro)
        {
            _logger.LogInformation("Rota {Endpoint} acionada", nameof(GetUsuarios));

            try
            {
                _logger.LogInformation("Rota {EndPoint} acionada", nameof(GetUsuarios));
                var resp = await _domain.GetUsuarios(filtro);
                
                return Ok(resp);
            }
            catch (System.NullReferenceException)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (System.TimeoutException)
            {
                return StatusCode(StatusCodes.Status408RequestTimeout);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            finally
            {
                _logger.LogInformation("Rota {Endpoint} finalizada", nameof(GetUsuarios));
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetUsuarioById([FromRoute] int id, [FromQuery] UsuarioFilter filtro)
        {
            _logger.LogInformation("Rota {Endpoint} acionada", nameof(GetUsuarioById));

            try
            {
                _logger.LogInformation("Rota {EndPoint} acionada", nameof(GetUsuarioById));
                var resp = await _domain.GetUsuarioById(id, filtro);

                if (resp == null) return BadRequest();
                
                return Ok(resp);
            }
            catch (System.NullReferenceException)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (System.TimeoutException)
            {
                return StatusCode(StatusCodes.Status408RequestTimeout);
            }
            catch (Exception)
            {
                return BadRequest();
            }
            finally
            {
                _logger.LogInformation("Rota {Endpoint} finalizada", nameof(GetUsuarioById));
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostUsuario([FromBody] Usuario usuario)
        {
            _logger.LogInformation("Rota {Endpoint} acionada", nameof(PostUsuario));

            try
            {
                _logger.LogInformation("Rota {EndPoint} acionada", nameof(PostUsuario));
                var resp = await _domain.AddUsuario(usuario);

                if (resp == null) return BadRequest();

                return Ok(resp);
            }
            catch (System.NullReferenceException)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (System.TimeoutException)
            {
                return StatusCode(StatusCodes.Status408RequestTimeout);
            }
            catch (Exception)
            {
                return BadRequest();
            }
            finally
            {
                _logger.LogInformation("Rota {Endpoint} finalizada", nameof(PostUsuario));
            }
        }
    }
}
