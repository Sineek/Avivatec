﻿using api_viagem.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using static api_viagem.Extensions.ViagemExtension;

namespace api_viagem.Repository.Interfaces
{
    public interface IViagemRepository
    {
        Task<IEnumerable<Viagem>> GetViagens(ViagemFilter filtro);
        Task<Viagem> GetViagemById(int id, ViagemFilter filtro = null);
        Task<IEnumerable<Viagem>> GetViagemByUserId(ViagemFilter filtro = null);
        Task<Viagem> Add(Viagem viagem);
    }
}
