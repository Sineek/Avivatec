﻿using api_viagem.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using static api_viagem.Extensions.UsuarioExtension;

namespace api_viagem.Repository.Interfaces
{
    public interface IUsuarioRepository
    {
        Task<IEnumerable<Usuario>> GetUsuarios(UsuarioFilter filtro);
        Task<Usuario> GetById(int id, UsuarioFilter filtro = null);
        Task<Usuario> Add(Usuario usuario);
    }
}
