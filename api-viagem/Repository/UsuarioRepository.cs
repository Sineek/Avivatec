﻿using api_viagem.Models;
using api_viagem.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using static api_viagem.Extensions.UsuarioExtension;

namespace api_viagem.Repository
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly Contexto _context;
        public UsuarioRepository(Contexto ctx)
        {
            _context = ctx;
        }

        public async Task <IEnumerable<Usuario>> GetUsuarios(UsuarioFilter filtro)
        {
            if (filtro.Includes == null)
                filtro.Includes = new List<string>();

            var usuarios = await _context.Usuario
                .AsNoTracking()
                .AplicaFiltro(filtro).ToListAsync(); ;

            return usuarios;
        }

        public async Task <Usuario> GetById(int id, UsuarioFilter filtro = null)
        {
            var model = await _context.Usuario
                .AsNoTracking()
                .AplicaFiltro(filtro)
                .SingleOrDefaultAsync(a => a.Id == id);

            return model;
        }

        public async Task<Usuario> Add(Usuario usuario)
        {
            var usr = new Usuario
            {
                Nome = usuario.Nome,
                Descricao = usuario.Descricao,
                Img = usuario.Img,
                Viagens = new List<Viagem>()
            };
            _context.Usuario.Add(usr);
            await _context.SaveChangesAsync();
            return usr;
        }
    }
}
