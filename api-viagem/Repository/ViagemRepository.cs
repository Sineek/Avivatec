﻿using api_viagem.Models;
using api_viagem.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using static api_viagem.Extensions.ViagemExtension;

namespace api_viagem.Repository
{
    public class ViagemRepository : IViagemRepository
    {
        private readonly Contexto _context;
        public ViagemRepository(Contexto ctx)
        {
            _context = ctx;
        }

        public async Task<IEnumerable<Viagem>> GetViagens(ViagemFilter filtro)
        {
            if (filtro.Includes == null)
                filtro.Includes = new List<string>();

            var usuarios = await _context.Viagem
                .AsNoTracking()
                .AplicaFiltro(filtro).ToListAsync(); ;

            return usuarios;
        }

        public async Task<Viagem> GetViagemById(int id, ViagemFilter filtro = null)
        {
            var model = await _context.Viagem
                .AsNoTracking()
                .AplicaFiltro(filtro)
                .SingleOrDefaultAsync(a => a.Id == id);

            return model;
        }

        public async Task<IEnumerable<Viagem>> GetViagemByUserId(ViagemFilter filtro = null)
        {
            var model = await _context.Viagem
                .AsNoTracking()
                .AplicaFiltro(filtro).ToListAsync();

            return model;
        }
        public async Task<Viagem> Add(Viagem viagem)
        {
            var vm = new Viagem
            {
                Destino = viagem.Destino,
                Origem = viagem.Origem,
                UsuarioId = viagem.UsuarioId,
                DataCriacao = viagem.DataCriacao
            };
            _context.Viagem.Add(vm);
            await _context.SaveChangesAsync();
            return vm;
        }
    }
}
