﻿using api_viagem.Domains.Interfaces;
using api_viagem.Models;
using api_viagem.Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using static api_viagem.Extensions.ViagemExtension;

namespace api_viagem.Domains
{
    public class ViagemDomain : IViagemDomain
    {
        private readonly IViagemRepository _repo;
        public ViagemDomain(IViagemRepository repo)
        {
            _repo = repo;
        }
        public async Task<IEnumerable<Viagem>> GetViagens(ViagemFilter filtro)
        {
            return await _repo.GetViagens(filtro);
        }
        public async Task<Viagem> GetViagemById(int id, ViagemFilter filtro)
        {
            return await _repo.GetViagemById(id, filtro);
        }
        public async Task<IEnumerable<Viagem>> GetViagemByUserId(ViagemFilter filtro)
        {
            return await _repo.GetViagemByUserId(filtro);
        }
        public async Task<Viagem> AddViagem(Viagem viagem)
        {
            return await _repo.Add(viagem);
        }
    }
}
