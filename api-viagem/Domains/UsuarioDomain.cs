﻿using api_viagem.Domains.Interfaces;
using api_viagem.Models;
using api_viagem.Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using static api_viagem.Extensions.UsuarioExtension;

namespace api_viagem.Domains
{
    public class UsuarioDomain : IUsuarioDomain
    {
        private readonly IUsuarioRepository _repo;
        public UsuarioDomain(IUsuarioRepository repo)
        {
            _repo = repo;

        }
        public async Task<IEnumerable<Usuario>> GetUsuarios(UsuarioFilter filtro)
        {
            return await _repo.GetUsuarios(filtro);
        }
        public async Task<Usuario> GetUsuarioById(int id, UsuarioFilter filtro)
        {
            return await _repo.GetById(id, filtro);
        }
        public async Task<Usuario> AddUsuario(Usuario usuario)
        {
            return await _repo.Add(usuario);
        }
    }
}