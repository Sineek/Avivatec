﻿using api_viagem.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using static api_viagem.Extensions.ViagemExtension;

namespace api_viagem.Domains.Interfaces
{
    public interface IViagemDomain
    {
        Task<IEnumerable<Viagem>> GetViagens(ViagemFilter filtro);
        Task<Viagem> GetViagemById(int id, ViagemFilter filtro);
        Task<IEnumerable<Viagem>> GetViagemByUserId(ViagemFilter filtro);
        Task<Viagem> AddViagem(Viagem viagem);
    }
}
