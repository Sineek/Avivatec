﻿using api_viagem.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using static api_viagem.Extensions.UsuarioExtension;

namespace api_viagem.Domains.Interfaces
{
    public interface IUsuarioDomain
    {
        Task<IEnumerable<Usuario>> GetUsuarios(UsuarioFilter filtro);
        Task<Usuario> GetUsuarioById(int id, UsuarioFilter filtro);
        Task<Usuario> AddUsuario(Usuario usuario);
    }
}
