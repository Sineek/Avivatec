using api_viagem.Domains;
using api_viagem.Domains.Interfaces;
using api_viagem.Models;
using api_viagem.Repository;
using api_viagem.Repository.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using System;


namespace api_viagem
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var stringConexao = "Server=localhost;DataBase=avivatec;Uid=root";
            var serverVersion = new MySqlServerVersion(new Version(8, 0, 21));
            services.AddControllers();
            services.AddMvc();
            services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:4200/");
                        builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
                    });
            });
            services.AddSwaggerGen(configure =>
            {
                configure.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "RickLocation", Version = "v1" });
              
            });
            services.AddDbContext<Contexto>(dbContextOptions => dbContextOptions.UseMySql(stringConexao, serverVersion));

            services.AddTransient<IUsuarioDomain, UsuarioDomain>();
            services.AddTransient<IViagemDomain, ViagemDomain>();

            services.AddTransient<IUsuarioRepository, UsuarioRepository>();
            services.AddTransient<IViagemRepository, ViagemRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(MyAllowSpecificOrigins);

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.RoutePrefix = "swagger";
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
            });
        }
    }
}
