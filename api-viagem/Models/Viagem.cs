﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace api_viagem.Models
{
    public class Viagem
    {
        public int Id { get; set; }

        public DateTimeOffset DataCriacao { get; set; }

        public string Origem { get; set; }

        public string Destino { get; set; }
        
        [ForeignKey("UsuarioId")]
        public int UsuarioId { get; set; }
    }
}
