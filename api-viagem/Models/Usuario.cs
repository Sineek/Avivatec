﻿using System.Collections.Generic;

namespace api_viagem.Models
{
    public class Usuario
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Img { get; set; }

        public string Descricao { get; set; }

        public IEnumerable<Viagem> Viagens { get; set; }
    }
}
