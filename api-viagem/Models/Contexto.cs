﻿using Microsoft.EntityFrameworkCore;

namespace api_viagem.Models
{
    public class Contexto : DbContext
    {
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Viagem> Viagem { get; set; }
        public Contexto(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Usuario>().ToTable("Usuario");
            builder.Entity<Viagem>().ToTable("Viagem");
            base.OnModelCreating(builder);
        }
    }
}
