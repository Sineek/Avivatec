import { Viagem } from "./viagem";

export class Usuario {
    id: number;
    nome: string;
    img: string;
    descricao: string;
    viagens: Viagem[];

    constructor(data?){
        if(data){
            this.id = data.id;
            this.nome = data.nome;
            this.img = data.img;
            this.descricao = data.descricao;
            this.viagens = [];

            if (data.viagens)
                this.viagens = data.viagens.map(x => new Viagem(x));
        }
    }
}
