export class Viagem {
    id: number;
    origem: string;
    destino: string;
    usuarioId: number;
    dataCriacao: Date;

    constructor(data?){
        if(data){
            this.id = data.id;
            this.destino = data.destino;
            this.origem = data.origem;
            this.usuarioId = data.usuarioId;
            this.dataCriacao = data.dataCriacao;
        }
    }
}
