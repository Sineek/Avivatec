import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Usuario } from 'src/app/models/usuario';
import { Viagem } from 'src/app/models/viagem';
import { UsuarioService } from 'src/app/services/usuario.service';
import { ViagemService } from 'src/app/services/viagem.service';

@Component({
  selector: 'app-historico-viagem',
  templateUrl: './historico-viagem.component.html',
  styleUrls: ['./historico-viagem.component.css']
})
export class HistoricoViagemComponent implements AfterViewInit {
  titulo: string = 'Serviço de Registro de Viagens';
  busca: string = "";
  displayedColumns: string[] = ['id', 'datav', 'origem', 'destino'];
  viagens: Viagem[] = [];
  dataSource = new MatTableDataSource<Viagem>(this.viagens);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  usuario: Usuario;
  idSelecionado: number;
  params = new HttpParams();

  constructor(public usuarioService: UsuarioService, public viagemService: ViagemService, private datePipe: DatePipe) { }
  
  ngAfterViewInit() {
    this.listagem();
  }

  listagem(){
    this.usuario = new Usuario();
    let params = new HttpParams();
    this.idSelecionado = this.usuarioService.getUsuario();
    params = params.set("Id", this.idSelecionado.toString());
    params = params.append("Includes", "Viagens");
    this.usuarioService.get(params)
    .then(dados =>{
      this.usuario = dados[0];
      this.viagens = this.usuario.viagens;
      this.dataSource = new MatTableDataSource<Viagem>(this.viagens);
      this.dataSource.paginator = this.paginator;
    })
  }

}
