import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricoViagemComponent } from './historico-viagem.component';

describe('HistoricoViagemComponent', () => {
  let component: HistoricoViagemComponent;
  let fixture: ComponentFixture<HistoricoViagemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoricoViagemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoViagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
