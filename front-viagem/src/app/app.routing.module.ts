import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PerfilComponent } from './perfil/perfil.component';
import { HistoricoViagemComponent } from './relatorios/historico-viagem/historico-viagem.component';
import { UsuarioComponent } from './usuario/usuario.component';

const appRoutes: Routes = [
    {
        path: '',
        component: UsuarioComponent
    },
    {
        path: 'perfil/historico-viagem',
        component: HistoricoViagemComponent
    },
    {
        path: 'perfil',
        component: PerfilComponent
    },

];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
