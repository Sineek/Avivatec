import { AfterViewInit, ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { Usuario } from '../models/usuario';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { Viagem } from '../models/viagem';
import { UsuarioService } from '../services/usuario.service';
import { HttpParams } from '@angular/common/http';
import { ViagemService } from '../services/viagem.service';
import { Router } from '@angular/router';
import { UsuarioModalComponent } from '../modal/usuario-modal/usuario-modal.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})


export class UsuarioComponent implements AfterViewInit {
  titulo: string = 'Bem vindos ao RickLocation, para começarmos, por favor escolha seu Rick';
  busca: string = "";
  viagens: Viagem[] = [];
  usuarios: Usuario[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<Usuario>(this.usuarios);
  observableData: Observable<any>;
  params = new HttpParams();
  usuarioSelecionado: number;
    
  constructor(public modal: MatDialog, private changeDetector : ChangeDetectorRef, private usuarioService: UsuarioService, private viagemService: ViagemService, private router: Router) {}

  ngAfterViewInit(): void {
    this.listagem();
    this.dataSource.paginator = this.paginator;
    this.observableData = this.dataSource.connect();
  }
  
  ngAfterViewChecked(){ this.changeDetector.detectChanges(); }

  listagem() {
    this.usuarioService.get(this.params)
    .then(dados => {
        this.usuarios = dados;
        this.dataSource = new MatTableDataSource<Usuario>(this.usuarios);
        this.observableData = this.dataSource.connect();
      })
  }

  atualizaStatus() {
      if(this.busca == ""){
        this.listagem();
      }else{
        this.params = this.params.set("Nome", this.busca);
        this.listagem();
        this.params = new HttpParams();
      }
  }
  
  verPerfil(usuarioSelecionado: number) {
    this.usuarioService.setUsuario(usuarioSelecionado);
    this.router.navigateByUrl('/perfil');
  }

  addUsuario(){
    const dialogRef = this.modal.open(UsuarioModalComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.listagem();
    });
  }

}
