import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ViagemComponent } from '../modal/viagem/viagem.component';
import { Usuario } from '../models/usuario';
import { Viagem } from '../models/viagem';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  titulo: string = "Bem vindo ao seu Perfil, jovem viajante!";
  viagens: Viagem[] = [];
  usuario: any;
  idSelecionado: number;

  constructor(public modal: MatDialog, public usuarioService: UsuarioService, private router: Router) { }

  ngOnInit(): void {
    this.usuario = new Usuario();
    let params = new HttpParams();
    this.idSelecionado = this.usuarioService.getUsuario();
    params = params.set("Id", this.idSelecionado.toString());
    this.usuarioService.get(params)
    .then(dados =>{
      this.usuario = dados[0];
    })
  }

  addViagem() {
    const dialogRef = this.modal.open(ViagemComponent);
    dialogRef.afterClosed().subscribe(result => {});
  }

  verHistorico(){
    this.usuarioService.setUsuario(this.usuario.id);
    this.router.navigateByUrl('/perfil/historico-viagem');
  }

}
