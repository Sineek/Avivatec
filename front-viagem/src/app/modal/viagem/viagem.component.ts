import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { Viagem } from 'src/app/models/viagem';
import { UsuarioService } from 'src/app/services/usuario.service';
import { ViagemService } from 'src/app/services/viagem.service';

@Component({
  selector: 'app-viagem',
  templateUrl: './viagem.component.html',
  styleUrls: ['./viagem.component.css']
})
export class ViagemComponent implements OnInit {
  viagem: Viagem;
  usuarioId: number;
  titulo: string = "Para onde iremos agora?";

  constructor(public viagemService: ViagemService, public usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.viagem = new Viagem();
    this.viagem.dataCriacao = new Date(), 'dd-MM-yyyy';
    this.usuarioId = this.usuarioService.getUsuario();
  }

  addViagem(){
    this.viagem.usuarioId = this.usuarioId;
    this.viagemService.create(this.viagem).then(
      success => {
        alert("Parabéns pela viagem!");
      },
      error => {
        alert("Eita, parece que deu algo errado no check-in, por favor tente novamente!");
      }
    );
  }

}
