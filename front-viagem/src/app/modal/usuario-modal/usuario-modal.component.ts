import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-usuario-modal',
  templateUrl: './usuario-modal.component.html',
  styleUrls: ['./usuario-modal.component.css']
})
export class UsuarioModalComponent implements OnInit {
  usuario: Usuario;
  titulo: string = "Cadastro de Usuário";

  constructor(public usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.usuario = new Usuario();
  }

  addUsuario(){
    this.usuarioService.create(this.usuario).then(
      success => {
        alert("Cadastrado com sucesso!");
      },
      error => {
        alert("Eita, parece que deu algo errado no cadastro, por favor tente novamente!");
      }
    );
  }

}
