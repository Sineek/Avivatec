import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class GenericService {

  constructor(protected http: HttpService,
    private API_URL) { }

  get(params?): Promise<any> {
      return this.http.get(this.API_URL, { params })
          .toPromise();
  }

  create(record): Promise<any> {
    return this.http.post(this.API_URL, record)
        .toPromise();
  }

}
