import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';
import { GenericService } from './generic.service';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ViagemService extends GenericService{
  constructor(http: HttpService) {
    super(http, '/Viagem/?');
  }
}