import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { urlApi } from 'src/environments/environment';
import { share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient){}

  get(url, options: any = {}): any {
    let header = (options.headers ? options.headers : new HttpHeaders());
    if (options.params) {
      return this.http.get(urlApi + url + options.params,{ headers: header });
    } else {
      return this.http.get(urlApi + url,{ headers: header });
    }
  }
  
  post(url, data) {
    return this.http.post(urlApi + url, data).pipe(share());
  }

}
