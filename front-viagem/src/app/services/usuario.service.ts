import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { GenericService } from './generic.service';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService extends GenericService {
  usuarioId: number;

  constructor(http: HttpService) {
    super(http, '/Usuario/?');
  }
  setUsuario(usr: number){
    this.usuarioId = usr;
  }
  getUsuario(){
    return this.usuarioId;
  }

}
